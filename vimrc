if &compatible
	set nocompatible " Old habits die hard...
endif

syntax on
filetype plugin indent on

set number relativenumber
set laststatus=1
set ruler
set hlsearch incsearch

set background=dark
