set guioptions -=m " Turn off menu bar
set guioptions -=T " Turn off toolbar
set guioptions -=r " Turn off scrollbar

" Use a base16 theme
packadd! base16-vim
" Use base16-material-palenight
colorscheme base16-material-palenight

" Use cursorline with gvim
set cursorline
